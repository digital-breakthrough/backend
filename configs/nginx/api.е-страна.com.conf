
# API
server {
    charset utf-8;
    client_max_body_size 128M;
    listen 80;

    server_name api.е-страна.com;
    root /var/www/е-страна.com/back/api/web;
    index index.php;

    access_log /var/www/е-страна.com/_log/nginx/access-api.log;
    error_log /var/www/е-страна.com/_log/nginx/error-api.log;

    location / {
        # Redirect everything that isn't a real file to index.php
        try_files $uri $uri/ /index.php$is_args$args;
    }

    # to avoid processing of calls to non-existing static files by Yii
    location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
        try_files $uri =404;
        expires 360d;
        # Не нужно засорять лог статикой
        access_log off;
    }

    # Если хотим красивую страницу 404
    # error_page 404 /404.html;

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;

        ## Cache
        # todo: С кешированием надо разобраться, что именно нам от него надо и надо ли
        # fastcgi_pass_header Cookie; # fill cookie valiables, $cookie_phpsessid for exmaple
        # fastcgi_ignore_headers Cache-Control Expires Set-Cookie; # Use it with caution because it is cause SEO problems
        # fastcgi_cache_key "$request_method|$server_addr:$server_port$request_uri|$cookie_phpsessid"; # generating unique key
        # fastcgi_cache fastcgi_cache; # use fastcgi_cache keys_zone
        # fastcgi_cache_path /tmp/nginx/ levels=1:2 keys_zone=fastcgi_cache:16m max_size=256m inactive=1d;
        # fastcgi_temp_path /tmp/nginx/temp 1 2; # temp files folder
        # fastcgi_cache_use_stale updating error timeout invalid_header http_500; # show cached page if error (even if it is outdated)
        # fastcgi_cache_valid 200 404 10s; # cache lifetime for 200 404;
        # or fastcgi_cache_valid any 10s; # use it if you want to cache any responses
    }

    location /upload/ {
        root /var/www/е-страна.com/back;
        location ~ \.(png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar|svg)$ {
            access_log off;
            expires 360d;
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';

            try_files $uri =404;
        }
    }

    location /swagger {
        alias /var/www/е-страна.com/back/api/web/swagger;
        index index.html;

        location ~ \.(css|js|json|yaml)$ {
            access_log off;
            expires off;
            try_files $uri =404;
        }
    }

    # Запрещаем запретное
    location ~ ^/assets/.*\.php$ {
        deny all;
    }

    location ~ /\.(ht|svn|git) {
        deny all;
    }

    location ~* /\. {
        deny all;
    }
}
