<?php

namespace common\services;

use Yii;
use socialpulse\centrifuge\JWTHelper;
use api\models\{LoginForm, PasswordResetRequestForm, ResetPasswordForm, SignupForm};

class AuthService
{
    /**
     * AuthService constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $user
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getJWTForCentrifuge($user): string
    {
        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT',
        ];

        $payload = [
            'id' => $user->id
        ];

        $secret = Yii::$app->get('centrifugo')->secret;

        return JWTHelper::generateJWT('sha256', $header, $payload, $secret);
    }

    /**
     * @param LoginForm $formModel
     * @param array $postParams
     * @return array
     * @throws \Throwable
     */
    public function SignIn(LoginForm $formModel, array $postParams)
    {
        $formModel->load($postParams, '');

        if ($formModel->validate() && $formModel->login()) {
            return Yii::$app->getUser()->getIdentity();
        }

        return [
            'result' => false,
            'errors' => $formModel->getErrors(),
        ];

    }

    /**
     * @param SignupForm $formModel
     * @param array $postParams
     * @return array
     * @throws \Throwable
     */
    public function SignUp(SignupForm $formModel, array $postParams)
    {
        if ($formModel->load($postParams, '')) {
            if ($user = $formModel->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return Yii::$app->getUser()->getIdentity();
                }
            }
        }

        return [
            'result' => false,
            'errors' => $formModel->getErrors(),
        ];

    }

    /**
     * @param PasswordResetRequestForm $formModel
     * @param array $postParams
     * @return array
     */
    public function PasswordResetRequest(PasswordResetRequestForm $formModel, array $postParams)
    {
        if ($formModel->load($postParams, '') && $formModel->validate()) {
            return [
                'result' => $formModel->sendEmail(),
            ];
        }

        return [
            'result' => false,
        ];
    }

    /**
     * @param ResetPasswordForm $formModel
     * @param array $postParams
     * @return array
     */
    public function ResetPassword(ResetPasswordForm $formModel, array $postParams)
    {
        if ($formModel->load(Yii::$app->request->post(), '') && $formModel->validate() && $formModel->resetPassword()) {
            return [
                'result' => true,
            ];
        }

        return [
            'result' => false,
            'errors' => $formModel->getErrors(),
        ];
    }
}