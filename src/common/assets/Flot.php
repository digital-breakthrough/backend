<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class Flot
 * @package common\assets
 */
class Flot extends AssetBundle
{
    public $sourcePath = '@bower/flot';
    public $js = [
        'jquery.flot.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
