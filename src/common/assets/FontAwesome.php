<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class FontAwesome
 * @package common\assets
 */
class FontAwesome extends AssetBundle
{
    public $sourcePath = '@bower/font-awesome';
    public $css = [
        'css/font-awesome.min.css'
    ];
}
