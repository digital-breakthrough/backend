<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class Html5shiv
 * @package common\assets
 */
class Html5shiv extends AssetBundle
{
    public $sourcePath = '@bower/html5shiv';
    public $js = [
        'dist/html5shiv.min.js'
    ];

    public $jsOptions = [
        'condition' => 'lt IE 9'
    ];
}
