<?php
/**
 * Created by PhpStorm.
 * User: nkolesnikov
 * Date: 11.04.19
 * Time: 17:33
 */

return [
    'mainRedmineProjectIds' => [
        328,
    ],
    'mainJiraConfigurationSchemeIds' => [
        10600,
    ],
];
