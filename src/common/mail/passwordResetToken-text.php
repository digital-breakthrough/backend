<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = env('MAIN_DOMAIN') . '/reset-password?token=' . $user->password_reset_token;
?>
Hello <?= $user->username ?>,

Follow the link below to reset your password:

<?= $resetLink ?>
