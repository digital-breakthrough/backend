<?php
return [
    'Sign me in' => 'Войти',
    'Sign Up' => 'Регистрация',
    'Sign me up' => 'Зарегистрироваться',
    'Username or Email' => 'Имя пользователя или e-mail',
    'Password' => 'Пароль',
    'I forgot my password' => '',
    'Register a new membership' => '',
    'Toggle navigation' => '',
    'You have {num} timeline items' => '',
    'View all' => 'Смотреть все',
    'You have {num} log items' => '',
    'Profile' => 'Профиль',
    'Login' => 'Вход',
    'Logout' => 'Выход',
    'Hello, {username}' => 'Привет, {username}',
    'Main' => '',
    'Catalogs' => 'Каталоги',
    'Statistic' => 'Статистика',
    'Timeline' => 'Хронология',
    'Accounts' => 'Аккаунты',
    'Activity' => 'Активность',
    'Publications' => 'Публикации',
    'Persons' => 'Люди',
    'Count persons' => 'Людей',
    'Count publications' => 'Публикаций',
    'Count reactions' => 'Реакций',
    'Reactions' => 'Реакция',
    'About' => 'О проекте',
    'Update Info' => 'Обновить информацию',
    'Update Keys' => 'Обновить ключи',
    'Enable' => 'Включить',
    'Disable' => 'Отключить',
    'Remove' => 'Удаить',




    'Account Settings' => 'Настройки аккаунта',
    'Articles' => 'Статьи',
    'Attachments' => 'Приложения',
    'Body' => 'Сообщение',
    'Check your email for further instructions.' => 'Проверьте ваш e-mail.',
    'Confirm Password' => 'Подтвердите пароль',
    'Contact' => 'Контакты',
    'E-mail' => 'E-mail',
    'Email' => 'Email',
    'Error while oauth process.' => 'Ошибка в процессе OAuth авторизации.',
    'Female' => 'Женский',
    'Home' => 'Главная',
    'If you forgot your password you can reset it <a href="{link}">here</a>' => 'Если вы забыли пароль, вы можете сбросить его <a href="{link}">здесь</a>',
    'Incorrect username or password.' => 'Неправильный логин или пароль.',
    'Language' => 'Язык',
    'Log in with' => 'Войти с помощью',

    'Male' => 'Мужской',
    'Name' => 'Имя',
    'Need an account? Sign up.' => 'Нужен аккаунт? Зарегистрируйтесь',
    'New password was saved.' => 'Новый пароль был сохранен',
    'Page not found' => 'Страница не найдена',

    'Password reset for {name}' => 'Сброс пароля для {name}',
    'Profile settings' => 'Настройки профиля',
    'Remember Me' => 'Запомнить меня',
    'Request password reset' => 'Запрос сброса пароля',
    'Reset password' => 'Сброс пароля',
    'Settings' => 'Настройки',
    'Sorry, we are unable to reset password for email provided.' => 'Извините, мы не можем сбросить пароль для этого e-mail.',
    'Subject' => 'Тема',
    'Submit' => 'Отправить',
    'Thank you for contacting us. We will respond to you as soon as possible.' => 'Спасибо. Мы свяжемся с Вами в ближайщее время',
    'There was an error sending email.' => 'Ошибка при отправке сообщения.',
    'This email address has already been taken.' => 'Этот e-mail уже занят',
    'This username has already been taken.' => 'Это имя пользователя уже занято',
    'Update' => 'Редактировать',
    'User Settings' => 'Настройки пользователя',
    'Username' => 'Имя пользователя',
    'Verification Code' => 'Проверочный код',
    'We already have a user with email {email}' => 'Пользователь с email {email} уже зарегистрирован.',
    'Welcome to {app-name}. Email with your login information was sent to your email.' => 'Добро пожаловать в {app-name}. E-mail с информацией о пользователе был отправлен на вашу почту.',
    'Your account has been successfully saved' => 'Ваш аккаунт был успешно сохранен',
    '{app-name} | Your login information' => '{app-name} | Информация о пользователе',
];
