<?php

namespace common\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @SWG\Definition(
 *   definition="Initiative",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property( property="id", type="integer", description="ID" ),
 *           @SWG\Property( property="consignment_id", type="integer", description="ID партии" ),
 *           @SWG\Property( property="name", type="string", description="Название" ),
 *           @SWG\Property( property="key", type="string", description="Уникальный ключ инициативы" ),
 *           @SWG\Property( property="created_at", type="string", description="Дата создания" ),
 *       )
 *   }
 * )
 *
 * @property int $id
 * @property int $consignment_id
 * @property string $name
 * @property int $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class Initiative extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%initiative}}';
    }

    /**
     * {@inheritdoc}
     */
    public function transactions()
    {
        return [
            'default' => self::OP_ALL,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_by'], 'default', 'value' => null],
            [['consignment_id', 'created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'key'], 'string', 'max' => 255],
            [['name', 'key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => false,
            ],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'key',
                ],
                'value' => function ($event) {
                    return uniqid();
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'consignment_id',
            'name',
            'key',
            'created_at',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function extraFields()
    {
        return [
            'creator',
            'users',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $user = Yii::$app->getUser()->getIdentity();
            $user->link('initiatives', $this);
        }

        $this->refresh();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable('initiative_member', ['initiative_id' => 'id']);
    }
}
