<?php

namespace common\models\voting;

use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * @SWG\Definition(
 *   definition="Voting",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property( property="id", type="integer", description="ID" ),
 *           @SWG\Property( property="name", type="string", description="Название" ),
 *           @SWG\Property( property="description", type="string", description="Описание" ),
 *           @SWG\Property( property="created_at", type="string", description="Дата создания" ),
 *       )
 *   }
 * )
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property VotingMember[] $votingMembers
 * @property User[] $users
 * @property VotingVote[] $votingVotes
 */
class Voting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'voting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function extraFields()
    {
        return [
            'members',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->refresh();
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getMembers()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable('voting_member', ['voting_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotingVotes()
    {
        return $this->hasMany(VotingVote::class, ['voting_id' => 'id']);
    }
}
