<?php

namespace common\models\voting;

use common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "voting_vote".
 *
 * @property int $voting_id
 * @property int $user_id
 * @property int $member_user_id
 * @property string $created_at
 *
 * @property User $user
 * @property User $memberUser
 * @property Voting $voting
 */
class VotingVote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'voting_vote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['voting_id', 'user_id', 'member_user_id'], 'default', 'value' => null],
            [['voting_id', 'user_id', 'member_user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['voting_id', 'user_id', 'member_user_id'], 'unique', 'targetAttribute' => ['voting_id', 'user_id', 'member_user_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['member_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['member_user_id' => 'id']],
            [['voting_id'], 'exist', 'skipOnError' => true, 'targetClass' => Voting::class, 'targetAttribute' => ['voting_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->refresh();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberUser()
    {
        return $this->hasOne(User::class, ['id' => 'member_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoting()
    {
        return $this->hasOne(Voting::class, ['id' => 'voting_id']);
    }
}
