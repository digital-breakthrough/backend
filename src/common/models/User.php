<?php

namespace common\models;

use common\models\consignment\Consignment;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * @SWG\Definition(
 *   definition="User",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property( property="id", type="integer", description="ID" ),
 *           @SWG\Property( property="key", type="string", description="Уникальный строковый ключ" ),
 *           @SWG\Property( property="email", type="string", description="E-mail" ),
 *           @SWG\Property( property="first_name", type="string", description="Имя" ),
 *           @SWG\Property( property="last_name", type="string", description="Фамилия" ),
 *           @SWG\Property( property="city", type="string", description="Город" ),
 *           @SWG\Property( property="birth_date", type="string", description="Дата рождения" ),
 *           @SWG\Property( property="marital_status", type="string", description="Семейное положение" ),
 *           @SWG\Property( property="political_status", type="string", description="Политические убеждения" ),
 *           @SWG\Property( property="consignments", type="array", description="Список партий, где состоит", @SWG\Items(ref="#/definitions/Consignment") ),
 *       )
 *   }
 * )
 *
 * User model
 *
 * @property integer $id
 * @property string $key
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $first_name // Имя
 * @property string $last_name // Фамилия
 * @property string $city // Город
 * @property string $birth_date // Дата рождения
 * @property string $marital_status // Семейное положение
 * @property string $political_status // Политические убеждения
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const ROLE_USER = 'user';
    const ROLE_MANAGER = 'manager';
    const ROLE_ADMINISTRATOR = 'admin';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'key',
                ],
                'value' => function ($event) {
                    return uniqid('', true);
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['key', 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        $fields = [
            'id',
            'email',
            'first_name',
            'last_name',
            'city',
            'birth_date',
            'marital_status',
            'political_status',
            'avatar' => function () {
                return 'https://placehold.it/100x100';
            },
            'consignments',
        ];

        if ($this->id === Yii::$app->getUser()->getIdentity()->getId()) {
            $fields = array_merge_recursive($fields, [
                'authKey',
//                'socketKey' => function () {
//                    return (new AuthService())->getJWTForCentrifuge($this);
//                },
            ]);
        }

        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function extraFields()
    {
        return [
            'status',
            'email',
            'initiatives',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token, 'status' => self::STATUS_ACTIVE]);
        //    throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function getDisplayName()
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getConsignments()
    {
        return $this->hasMany(Consignment::class, ['id' => 'consignment_id'])
            ->viaTable('consignment_member', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getInitiatives()
    {
        return $this->hasMany(Initiative::class, ['id' => 'initiative_id'])
            ->viaTable('initiative_member', ['user_id' => 'id']);
    }
}
