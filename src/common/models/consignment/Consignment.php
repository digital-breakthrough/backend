<?php

namespace common\models\consignment;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\models\Initiative;
use common\models\User;

/**
 * @SWG\Definition(
 *   definition="Consignment",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property( property="id", type="integer", description="ID" ),
 *           @SWG\Property( property="name", type="string", description="Название" ),
 *           @SWG\Property( property="key", type="string", description="Уникальный ключ партии (используем его для реферальной ссылки)" ),
 *           @SWG\Property( property="created_at", type="string", description="Дата создания" ),
 *       )
 *   }
 * )
 *
 * @property int $id
 * @property string $name
 * @property int $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class Consignment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%consignment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function transactions()
    {
        return [
            'default' => self::OP_ALL,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_by'], 'default', 'value' => null],
            [['created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'key'], 'string', 'max' => 255],
            [['name', 'key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => false,
            ],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'key',
                ],
                'value' => function ($event) {
                    return uniqid();
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'name',
            'key',
            'created_at',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function extraFields()
    {
        return [
            'creator',
            'members',
            'initiatives',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $user = Yii::$app->getUser()->getIdentity();
            $user->link('consignments', $this);
        }

        $this->refresh();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getMembers()
    {
        return $this->hasMany(ConsignmentMember::class, ['consignment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getInitiatives()
    {
        return $this->hasMany(Initiative::class, ['id' => 'consignment_id']);
    }
}
