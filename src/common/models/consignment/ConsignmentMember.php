<?php

namespace common\models\consignment;

use common\models\User;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * @SWG\Definition(
 *   definition="ConsignmentMember",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           @SWG\Property( property="consignment_id", type="integer", description="ID Партии" ),
 *           @SWG\Property( property="referrer_id", type="integer", description="ID пригласившего пользователя" ),
 *           @SWG\Property( property="user_id", type="integer", description="ID пользователя" ),
 *           @SWG\Property( property="created_at", type="string", description="Дата создания" ),
 *       )
 *   }
 * )
 *
 * @property int $consignment_id
 * @property int $referrer_id
 * @property int $user_id
 * @property string $created_at
 *
 * @property User $referrer
 * @property User $user
 */
class ConsignmentMember extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%consignment_member}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['consignment_id', 'referrer_id', 'user_id'], 'required'],
            [['consignment_id', 'referrer_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'user' => function () {
                return $this->user;
            },
            'referrer' => function () {
                return $this->referrer;
            },
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsignment()
    {
        return $this->hasOne(Consignment::class, ['id' => 'consignment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getReferrer()
    {
        return $this->hasOne(User::class, ['id' => 'referrer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
