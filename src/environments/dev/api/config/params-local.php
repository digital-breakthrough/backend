<?php
return [
    'accessControlAllowOrigin' => [
        '127.0.0.1',
        'http://127.0.0.1',
        'http://localhost:3000',
        'http://e-strana.hack',
        'http://xn----8sbar6bqgi.com',
        'https://xn----8sbar6bqgi.com',
        'http://194.67.90.225',
        'https://194.67.90.225',
    ],
];
