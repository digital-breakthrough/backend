<?php
return [
    'adminEmail' => 'admin@example.com',

    // Задачи, запускаемые кроном
    'cronJobs' => require(__DIR__ . '/cron-jobs.php'),
];
