<?php

return [
    'schedule/test' => [
        'cron' => '* * * * *',
    ],

    'schedule/check-jira-projects' => [
        'cron' => '* * * * *',
    ],

    'schedule/check-redmine-projects' => [
        'cron' => '* * * * *',
    ],

    'schedule/check-redmine-statuses' => [
        'cron' => '0 0 * * *',
    ],


//    'schedule/check-redmine-projects-members' => [
//        'cron' => '* * * * *',
//    ],

//    'schedule/check-jira-projects-fields-schemes' => [
//        'cron' => '* * * * *',
//    ],

//    'schedule/check-new-jira-issues' => [
//        'cron' => '* * * * *',
//    ],

//    'schedule/check-jira-issues' => [
//        'cron' => '* * * * *',
//    ],

//    'schedule/check-jira-comments' => [
//        'cron' => '* * * * *',
//    ],

//    'schedule/check-redmine-issue' => [
//        'cron' => '* * * * *',
//    ],

];
