<?php

use yii\db\Migration;

/**
 * Class m190706_202955_create_voting
 */
class m190706_202955_create_voting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%voting}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),

            'created_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
            'updated_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%voting}}');
    }
}
