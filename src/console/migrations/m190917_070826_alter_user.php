<?php

use yii\db\Migration;

/**
 * Class m190917_070826_alter_user
 */
class m190917_070826_alter_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'first_name', $this->string());
        $this->addColumn('{{%user}}', 'last_name', $this->string());
        $this->addColumn('{{%user}}', 'city', $this->string());
        $this->addColumn('{{%user}}', 'birth_date', $this->timestamp()->append('with time zone'));
        $this->addColumn('{{%user}}', 'marital_status', $this->string());
        $this->addColumn('{{%user}}', 'political_status', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'first_name');
        $this->dropColumn('{{%user}}', 'last_name');
        $this->dropColumn('{{%user}}', 'city');
        $this->dropColumn('{{%user}}', 'birth_date');
        $this->dropColumn('{{%user}}', 'marital_status');
        $this->dropColumn('{{%user}}', 'political_status');
    }
}
