<?php

use yii\db\Migration;

/**
 * Class m190706_095213_create_consignment_member
 */
class m190706_095213_create_consignment_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%consignment_member}}', [
            'consignment_id' => $this->integer(),
            'user_id' => $this->integer(),
            'created_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
        ], $tableOptions);

        $this->addForeignKey('fk_consignment_member__consignment', '{{%consignment_member}}', 'consignment_id', '{{%consignment}}', 'id');
        $this->addForeignKey('fk_consignment_member__user', '{{%consignment_member}}', 'user_id', '{{%user}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%consignment_member}}');
    }
}
