<?php

use yii\db\Migration;

/**
 * Class m190706_095206_create_consignment
 */
class m190706_095206_create_consignment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%consignment}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'key' => $this->string(),

            'created_by' => $this->integer(),
            'created_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
            'updated_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%consignment}}');
    }
}
