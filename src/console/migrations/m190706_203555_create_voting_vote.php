<?php

use yii\db\Migration;

/**
 * Class m190706_203555_create_voting_vote
 */
class m190706_203555_create_voting_vote extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%voting_vote}}', [
            'voting_id' => $this->integer(),
            'user_id' => $this->integer(),
            'member_user_id' => $this->integer(),
            'created_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
        ], $tableOptions);

        $this->addForeignKey('fk_voting_vote__voting', '{{%voting_vote}}', 'voting_id', '{{%voting}}', 'id');
        $this->addForeignKey('fk_voting_vote__user', '{{%voting_vote}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('fk_voting_vote__member', '{{%voting_vote}}', 'member_user_id', '{{%user}}', 'id');

        $this->createIndex('un_voting_vote', 'voting_vote', ['voting_id', 'user_id', 'member_user_id'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%voting_vote}}');
    }
}
