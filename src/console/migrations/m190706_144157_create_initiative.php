<?php

use yii\db\Migration;

/**
 * Class m190706_144157_create_initiative
 */
class m190706_144157_create_initiative extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%initiative}}', [
            'id' => $this->primaryKey(),
            'consignment_id' => $this->integer(),
            'name' => $this->string(),
            'key' => $this->string(),

            'created_by' => $this->integer(),
            'created_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
            'updated_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%initiative}}');
    }
}
