<?php

use yii\db\Migration;

/**
 * Class m190706_192438_alter_user
 */
class m190706_192438_alter_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'key', $this->string(50)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'key');
    }
}
