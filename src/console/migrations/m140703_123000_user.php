<?php

use yii\db\Migration;
use common\models\User;

class m140703_123000_user extends Migration
{
    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger(),
            'created_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
            'updated_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
        ], $tableOptions);


    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
