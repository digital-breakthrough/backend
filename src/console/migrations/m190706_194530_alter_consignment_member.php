<?php

use yii\db\Migration;

/**
 * Class m190706_194530_alter_consignment_member
 */
class m190706_194530_alter_consignment_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%consignment_member}}', 'referrer_id', $this->integer()->after('user_id'));
        $this->addForeignKey('fk_consignment_member__referrer', '{{%consignment_member}}', 'referrer_id', '{{%user}}', 'id');

        $this->createIndex('un_consignment_member', 'consignment_member', ['consignment_id', 'referrer_id', 'user_id'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%consignment_member}}', 'referrer_id');
    }
}
