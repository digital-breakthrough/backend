<?php

use yii\db\Migration;

/**
 * Class m190706_161021_create_initiative_member
 */
class m190706_161021_create_initiative_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%initiative_member}}', [
            'initiative_id' => $this->integer(),
            'user_id' => $this->integer(),
            'created_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
        ], $tableOptions);

        $this->addForeignKey('fk_initiative_member__initiative', '{{%initiative_member}}', 'initiative_id', '{{%initiative}}', 'id');
        $this->addForeignKey('fk_initiative_member__user', '{{%initiative_member}}', 'user_id', '{{%user}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%initiative_member}}');
    }
}
