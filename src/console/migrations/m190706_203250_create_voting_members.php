<?php

use yii\db\Migration;

/**
 * Class m190706_203250_create_voting_members
 */
class m190706_203250_create_voting_members extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%voting_member}}', [
            'voting_id' => $this->integer(),
            'user_id' => $this->integer(),
            'created_at' => $this->timestamp()->append('with time zone DEFAULT now()'),
        ], $tableOptions);

        $this->createIndex('un_voting_member', 'voting_member', ['voting_id', 'user_id'], true);

        $this->addForeignKey('fk_voting_member__voting', '{{%voting_member}}', 'voting_id', '{{%voting}}', 'id');
        $this->addForeignKey('fk_voting_member__user', '{{%voting_member}}', 'user_id', '{{%user}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%voting_members}}');
    }
}
