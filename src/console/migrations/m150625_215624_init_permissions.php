<?php

use yii\db\Migration;
use common\models\User;

class m150625_215624_init_permissions extends Migration
{
    public function up()
    {
        $auth = Yii::$app->get('authManager');
        $userRole = $auth->createRole(User::ROLE_USER);
        $managerRole = $auth->createRole(User::ROLE_MANAGER);
        $administratorRole = $auth->createRole(User::ROLE_ADMINISTRATOR);

        $auth->add($userRole);
        $auth->add($managerRole);
        $auth->add($administratorRole);
    }

    public function down()
    {
        $auth = Yii::$app->get('authManager');
        $auth->remove($auth->getRole(User::ROLE_USER));
        $auth->remove($auth->getRole(User::ROLE_MANAGER));
        $auth->remove($auth->getRole(User::ROLE_ADMINISTRATOR));
    }
}
