<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/admin-lte-fix.css',
        'css/main.css',
    ];
    public $js = [
        'js/app.js',
//        'js/sockets.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'common\assets\AdminLte',
        'common\assets\Html5shiv',
        'common\assets\FontAwesome',
        'frontend\assets\BowerAsset',
    ];
}