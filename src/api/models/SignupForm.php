<?php

namespace api\models;

use common\models\consignment\Consignment;
use yii\base\Model;
use common\models\User;

/**
 * @SWG\Definition(
 *   definition="SignupForm",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(
 *           required={"email", "password"},
 *          @SWG\Property(
 *              property="email",
 *              type="string",
 *              maxLength=255,
 *              description="Емайл"
 *           ),
 *           @SWG\Property(
 *              property="password",
 *              type="string",
 *              minLength=6,
 *              description="Пароль"
 *           ),
 *           @SWG\Property(
 *              property="code",
 *              type="string",
 *              description="Реферальный код партии"
 *           ),
 *           @SWG\Property(
 *              property="first_name",
 *              type="string",
 *              description="Имя"
 *           ),
 *           @SWG\Property(
 *              property="last_name",
 *              type="string",
 *              description="Фамилия"
 *           ),
 *           @SWG\Property(
 *              property="city",
 *              type="string",
 *              description="Город"
 *           ),
 *           @SWG\Property(
 *              property="birth_date",
 *              type="string",
 *              description="Дата рождения"
 *           ),
 *           @SWG\Property(
 *              property="marital_status",
 *              type="string",
 *              description="Семейное положение"
 *           ),
 *           @SWG\Property(
 *              property="political_status",
 *              type="string",
 *              description="Политические убеждения"
 *           ),
 *       )
 *   }
 * )
 *
 * Signup Form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $code;

    public $first_name;
    public $last_name;
    public $city;
    public $birth_date;
    public $marital_status;
    public $political_status;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],

            [['first_name', 'last_name', 'city', 'birth_date', 'marital_status', 'political_status'], 'trim'],
            [['first_name', 'last_name', 'city', 'birth_date', 'marital_status', 'political_status'], 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'string', 'min' => 6],

            ['code', 'trim'],
            ['code', 'string'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws \yii\base\Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->city = $this->city;
        $user->birth_date = $this->birth_date;
        $user->marital_status = $this->marital_status;
        $user->political_status = $this->political_status;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($user->save()) {
            $consignment = Consignment::findOne(['key' => $this->code]);

            if ($consignment) {
                $user->link('consignments', $consignment);
            }

            return $user;
        }

        return null;
    }
}
