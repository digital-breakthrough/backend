<?php

return [
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'auth',
        'pluralize' => false,
        'extraPatterns' => [
            'POST,OPTIONS login' => 'login',
            'POST,OPTIONS sign-up' => 'sign-up',
            'POST,OPTIONS request-password-reset' => 'request-password-reset',
            'POST,OPTIONS reset-password' => 'reset-password',
        ],
        'except' => ['index', 'view', 'delete', 'create', 'update'],
    ],

    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'user',
        'pluralize' => false,
        'extraPatterns' => [
            'GET,OPTIONS me' => 'me',
            'POST,OPTIONS me/consignment/<codeReferrer:\w+>-<codeConsignment:\w+>' => 'join-consignment',
            'DELETE,OPTIONS me/consignment/<code:\w+>' => 'leave-consignment',
            'POST,OPTIONS me/initiative/<code:\w+>' => 'join-initiative',
            'POST,OPTIONS me/comment/<idNews:\d+>' => 'comment-news',
        ],
    ],

    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'consignment',
        'pluralize' => false,
        'extraPatterns' => [
            'GET,OPTIONS <id:\d+>/news' => 'news',
            'POST,OPTIONS <id:\d+>/news' => 'create-news',
        ],
    ],

    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'initiative',
        'pluralize' => false,
    ],

    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'voting',
        'pluralize' => false,
        'extraPatterns' => [
            'POST,OPTIONS <idVote:\d+>/vote/<idMember:\d+>' => 'vote',
            'POST,OPTIONS <id:\d+>/member' => 'create-member',
        ],
    ],
];
