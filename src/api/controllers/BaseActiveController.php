<?php

namespace api\controllers;

use Yii;
use yii\filters\Cors;
use yii\filters\auth\{
    CompositeAuth,
    HttpBearerAuth,
    QueryParamAuth
};
use yii\rest\ActiveController;

/**
 * Class BaseActiveController
 * @package api\controllers
 */
class BaseActiveController extends ActiveController
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => Cors::class,
                'cors' => [
                    'Origin' => \Yii::$app->params['accessControlAllowOrigin'],
                    'Access-Control-Allow-Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['Origin', 'Content-Type', 'Authorization'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Expose-Headers' => ['*'],
                    'Access-Control-Max-Age' => 60 * 60,
                ],
            ],
            'authenticator' => [
                'class' => CompositeAuth::class,
                'authMethods' => [
                    HttpBearerAuth::class,
                    QueryParamAuth::class,
                ],
            ],
        ];
    }
}
