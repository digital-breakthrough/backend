<?php

namespace api\controllers;

use common\models\consignment\Consignment;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * Class ConsignmentController
 * @package api\controllers
 */
class ConsignmentController extends BaseActiveController
{
    public $modelClass = Consignment::class;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * @SWG\Get(
     *     path="/consignment",
     *     summary="Получение списка партий",
     *     produces={"application/json", "application/xml"},
     *     tags={"Consignment"},
     *     @SWG\Parameter(
     *         name="expand",
     *         in="query",
     *         description="
     *          Подгрузить дополнительные данные:
     *          creator - Создатель партии
     *          members - Члены партии
     *          initiatives - Инициативы",
     *         required=false,
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             enum={
     *                "creator",
     *                "members",
     *                "initiatives",
     *             }
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает список партий",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Consignment")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */

    /**
     * @SWG\Get(
     *     path="/consignment/{id}",
     *     summary="Получение информации о партии",
     *     produces={"application/json", "application/xml"},
     *     tags={"Consignment"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID партии",
     *         type="integer",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="expand",
     *         in="query",
     *         description="
     *          Подгрузить дополнительные данные:
     *          creator - Создатель партии
     *          members - Члены партии
     *          initiatives - Инициативы",
     *         required=false,
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             enum={
     *                "creator",
     *                "members",
     *                "initiatives",
     *             }
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает информацию о партии",
     *         @SWG\Schema(ref="#/definitions/Consignment"),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */

    /**
     * @SWG\Post(
     *     path="/consignment",
     *     summary="Создание партии",
     *     produces={"application/json", "application/xml"},
     *     tags={"Consignment"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Параметры инициативы",
     *         required=true,
     *         @SWG\Schema(
     *             @SWG\Property(
     *                 property="name",
     *                 type="string"
     *             ),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Возвращает информацию о партии",
     *         @SWG\Schema(ref="#/definitions/Consignment"),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */

    /**
     * @SWG\Put(
     *     path="/consignment/{id}",
     *     summary="Редактирование партии",
     *     produces={"application/json", "application/xml"},
     *     tags={"Consignment"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID партии",
     *         type="integer",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Параметры партии",
     *         required=true,
     *         @SWG\Schema(
     *             @SWG\Property(
     *                 property="name",
     *                 type="string"
     *             ),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает информацию о партии",
     *         @SWG\Schema(ref="#/definitions/Consignment"),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */

    /**
     * @SWG\Delete(
     *     path="/consignment/{id}",
     *     summary="Удаление партии",
     *     produces={"application/json", "application/xml"},
     *     tags={"Consignment"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID партии",
     *         type="integer",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Все хорошо",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */

    /**
     * @SWG\Get(
     *     path="/consignment/{id}/news",
     *     summary="Получение списка новостей партии",
     *     produces={"application/json", "application/xml"},
     *     tags={"Consignment"},
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает список новостей",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Consignment")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */
    public function actionNews($id)
    {
        return '!!!';
    }

    /**
     * @SWG\Post(
     *     path="/consignment/{id}/news",
     *     summary="Создание новости в партии",
     *     produces={"application/json", "application/xml"},
     *     tags={"Consignment"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Параметры инициативы",
     *         required=true,
     *         @SWG\Schema(
     *             @SWG\Property(
     *                 property="name",
     *                 type="string"
     *             ),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Ок",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */
    public function actionCreateNews($id)
    {
        return '!!!';
    }
}
