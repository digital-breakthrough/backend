<?php

namespace api\controllers;

use common\models\Initiative;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * Class InitiativeController
 * @package api\controllers
 */
class InitiativeController extends BaseActiveController
{
    public $modelClass = Initiative::class;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * @SWG\Get(
     *     path="/initiative",
     *     summary="Получение списка инициатив",
     *     produces={"application/json", "application/xml"},
     *     tags={"Initiative"},
     *     @SWG\Parameter(
     *         name="expand",
     *         in="query",
     *         description="
     *          Подгрузить дополнительные данные:
     *          creator - Создатель инициативы
     *          users - Подписавшиеся пользователи",
     *         required=false,
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             enum={
     *                "creator",
     *                "users",
     *             }
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает список инициатив",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Initiative")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */

    /**
     * @SWG\Get(
     *     path="/initiative/{id}",
     *     summary="Получение информации об инициативе",
     *     produces={"application/json", "application/xml"},
     *     tags={"Initiative"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID инициативы",
     *         type="integer",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="expand",
     *         in="query",
     *         description="
     *          Подгрузить дополнительные данные:
     *          creator - Создатель инициативы
     *          users - Подписавшиеся пользователи",
     *         required=false,
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             enum={
     *                "creator",
     *                "users",
     *             }
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает информацию об инициативе",
     *         @SWG\Schema(ref="#/definitions/Initiative"),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */

    /**
     * @SWG\Post(
     *     path="/initiative",
     *     summary="Создание инициативы",
     *     produces={"application/json", "application/xml"},
     *     tags={"Initiative"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Параметры инициативы",
     *         type="array",
     *         required=true,
     *         @SWG\Schema(
     *             @SWG\Property(
     *                 property="name",
     *                 type="string"
     *             ),
     *             @SWG\Property(
     *                 property="consignment_id",
     *                 type="integer",
     *                 description="ID партии"
     *             ),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Возвращает информацию об инициативе",
     *         @SWG\Schema(ref="#/definitions/Initiative"),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */

    /**
     * @SWG\Put(
     *     path="/initiative/{id}",
     *     summary="Редактирование инициативы",
     *     produces={"application/json", "application/xml"},
     *     tags={"Initiative"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID инициативы",
     *         type="integer",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Параметры инициативы",
     *         required=true,
     *         @SWG\Schema(
     *             @SWG\Property(
     *                 property="name",
     *                 type="string"
     *             ),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает информацию об инициативе",
     *         @SWG\Schema(ref="#/definitions/Initiative"),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */

    /**
     * @SWG\Delete(
     *     path="/initiative/{id}",
     *     summary="Удаление инициативы",
     *     produces={"application/json", "application/xml"},
     *     tags={"Initiative"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID инициативы",
     *         type="integer",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Все хорошо",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */
}
