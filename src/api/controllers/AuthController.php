<?php

namespace api\controllers;

use Throwable;
use Yii;
use api\models\{LoginForm, PasswordResetRequestForm, ResetPasswordForm, SignupForm};
use common\services\AuthService;

/**
 * Class AuthController
 * @package api\controllers
 */
class AuthController extends ApiController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        if (array_key_exists('authenticator', $behaviors)) {
            unset($behaviors['authenticator']);
        }

        return $behaviors;
    }

    /**
     * @SWG\Post(
     *     path="/auth/login",
     *     summary="Авторизация пользователя",
     *     produces={"application/json", "application/xml"},
     *     tags={"Auth"},
     *     @SWG\Parameter(
     *             name="body",
     *             in="body",
     *             description="Данные",
     *             required=true,
     *             @SWG\Schema(ref="#/definitions/LoginForm"),
     *         ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает авторизованного пользователя",
     *         @SWG\Schema(ref="#/definitions/User"),
     *     )
     * )
     *
     * @return array
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogin()
    {
        return (new AuthService())->SignIn(new LoginForm(), Yii::$app->request->post());
    }

    /**
     * @SWG\Post(
     *     path="/auth/sign-up",
     *     summary="Регистрация пользователя",
     *     produces={"application/json"},
     *     tags={"Auth"},
     *     @SWG\Parameter(
     *             name="body",
     *             in="body",
     *             description="Данные",
     *             required=true,
     *             @SWG\Schema(ref="#/definitions/SignupForm"),
     *         ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает авторизованного пользователя",
     *         @SWG\Schema(ref="#/definitions/User"),
     *     )
     * )
     *
     * @return array
     * @throws Throwable
     */
    public function actionSignUp()
    {
        return (new AuthService())->SignUp(new SignupForm(), Yii::$app->request->post());
    }

    /**
     * @SWG\Post(
     *     path="/auth/request-password-reset",
     *     summary="Отправка сообщения для сброса пароля",
     *     produces={"application/json"},
     *     tags={"Auth"},
     *     @SWG\Parameter(
     *             name="body",
     *             in="body",
     *             description="Данные",
     *             required=true,
     *             @SWG\Schema(ref="#/definitions/PasswordResetRequestForm"),
     *         ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает true",
     *     )
     * )
     *
     * @return array
     */
    public function actionRequestPasswordReset()
    {
        return (new AuthService())->PasswordResetRequest(new PasswordResetRequestForm(), Yii::$app->request->post());
    }

    /**
     * @SWG\Post(
     *     path="/auth/reset-password",
     *     summary="Отправка запроса на смену пароля",
     *     produces={"application/json"},
     *     tags={"Auth"},
     *     @SWG\Parameter(
     *             name="body",
     *             in="body",
     *             description="Данные",
     *             required=true,
     *             @SWG\Schema(ref="#/definitions/ResetPasswordForm"),
     *         ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает результат изменения пароля",
     *     )
     * )
     *
     * @return array
     */
    public function actionResetPassword()
    {
        try {
            //todo надо переделать логику, чтобы загружало и проверяло в validate, а не в конструкторе
            $formModel = new ResetPasswordForm(Yii::$app->request->post('token'));
        } catch (Throwable $e) {
            return [
                'result' => false,
                'errors' => ['token' => $e->getMessage()],
            ];
        }

        return (new AuthService())->ResetPassword($formModel, Yii::$app->request->post());
    }
}
