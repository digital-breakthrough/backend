<?php

namespace api\controllers;

use common\models\consignment\Consignment;
use common\models\consignment\ConsignmentMember;
use common\models\Initiative;
use common\models\voting\Voting;
use common\models\voting\VotingVote;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use common\models\User;
use yii\web\NotFoundHttpException;

/**
 * Class UserController
 * @package api\controllers
 */
class UserController extends ApiController
{
    public $modelClass = User::class;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * @SWG\Get(
     *     path="/user/me",
     *     summary="Получение информации о авторизованном пользователе",
     *     produces={"application/json", "application/xml"},
     *     tags={"User"},
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает авторизованного пользователя",
     *         @SWG\Schema(ref="#/definitions/User"),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     security={{"bearer":{}}}
     * )
     *
     * @return array
     * @throws \Throwable
     */
    public function actionMe()
    {
        return Yii::$app->getUser()->getIdentity();
    }

    /**
     * @SWG\Post(
     *     path="/user/me/consignment/{codeReferrer}-{codeConsignment}",
     *     summary="Вступить в партию",
     *     produces={"application/json", "application/xml"},
     *     tags={"User"},
     *     @SWG\Parameter(
     *         name="codeReferrer",
     *         in="path",
     *         description="Код пригласившего пользователя",
     *         type="string",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="codeConsignment",
     *         in="path",
     *         description="Код партии",
     *         type="string",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="expand",
     *         in="query",
     *         description="
     *          Подгрузить дополнительные данные:
     *          email - Email пользователя
     *          initiatives - Инициативы, созданные пользователем",
     *         required=false,
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             enum={
     *                "email",
     *                "initiatives",
     *             }
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         @SWG\Schema(ref="#/definitions/User"),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Consignment not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     *
     * @return array
     * @throws \Throwable
     */
    public function actionJoinConsignment($codeReferrer, $codeConsignment)
    {
        $user = Yii::$app->getUser()->getIdentity();
        $consignment = Consignment::findOne(['key' => $codeConsignment]);
        $referrer = User::findOne(['key' => $codeReferrer]);

        if (!$consignment) {
            throw new NotFoundHttpException('Consignment not found');
        }

        if ($user && $referrer && $consignment) {
            $consignmentMember = ConsignmentMember::findOne([
                'consignment_id' => $consignment->id,
                'referrer_id' => $referrer->id,
                'user_id' => $user->id,
            ]);

            if (!$consignmentMember) {
                $consignmentMember = new ConsignmentMember([
                    'consignment_id' => $consignment->id,
                    'referrer_id' => $referrer->id,
                    'user_id' => $user->id,
                ]);
                $consignmentMember->save();
            } else {
                return false;
            }
        }

        return $user;
    }

    /**
     * @SWG\Delete(
     *     path="/user/me/consignment/{code}",
     *     summary="Выйти из партии",
     *     produces={"application/json", "application/xml"},
     *     tags={"User"},
     *     @SWG\Parameter(
     *         name="code",
     *         in="path",
     *         description="Код партии",
     *         type="string",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         @SWG\Schema(ref="#/definitions/User"),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Consignment not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     *
     * @return array
     * @throws \Throwable
     */
    public function actionLeaveConsignment($code)
    {
        $user = Yii::$app->getUser()->getIdentity();
        $consignment = Consignment::findOne(['key' => $code]);

        if (!$consignment) {
            throw new NotFoundHttpException('Consignment not found');
        }

        if ($user && $consignment) {
            $user->unlinkAll('consignments', $consignment);
        }

        return $user;
    }

    /**
     * @SWG\Post(
     *     path="/user/me/initiative/{code}",
     *     summary="Поддержать инициативу",
     *     produces={"application/json", "application/xml"},
     *     tags={"User"},
     *     @SWG\Parameter(
     *         name="code",
     *         in="path",
     *         description="Код инициативы",
     *         type="string",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="",
     *         @SWG\Schema(ref="#/definitions/User"),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Consignment not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     *
     * @return array
     * @throws \Throwable
     */
    public function actionJoinInitiative($code)
    {
        $user = Yii::$app->getUser()->getIdentity();
        $initiative = Initiative::findOne(['key' => $code]);

        if (!$initiative) {
            throw new NotFoundHttpException('Initiative not found');
        }

        if ($user && $initiative) {
            $user->link('initiatives', $initiative);
        }

        return $user;
    }

    /**
     * @SWG\Post(
     *     path="/user/me/comment/{id}",
     *     summary="Комментировать новость",
     *     produces={"application/json", "application/xml"},
     *     tags={"User"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID новости",
     *         type="integer",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Ok",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="News not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     *
     * @return array
     * @throws \Throwable
     */
    public function actionCommentNews($code)
    {
        return '!!!';
    }
}
