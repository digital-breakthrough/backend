<?php

namespace api\controllers;

use common\models\voting\Voting;
use common\models\voting\VotingVote;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class VotingController
 * @package api\controllers
 */
class VotingController extends BaseActiveController
{
    public $modelClass = Voting::class;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * @SWG\Get(
     *     path="/voting",
     *     summary="Получение списка выборов",
     *     produces={"application/json", "application/xml"},
     *     tags={"Voting"},
     *     @SWG\Parameter(
     *         name="expand",
     *         in="query",
     *         description="
     *          Подгрузить дополнительные данные:
     *          members - Кандидаты",
     *         required=false,
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             enum={
     *                "members",
     *             }
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает список голосований",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Voting")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */

    /**
     * @SWG\Get(
     *     path="/voting/{id}",
     *     summary="Получение информации о голосовании",
     *     produces={"application/json", "application/xml"},
     *     tags={"Voting"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID голосования",
     *         type="integer",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="expand",
     *         in="query",
     *         description="
     *          Подгрузить дополнительные данные:
     *          members - Кандидаты",
     *         required=false,
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             enum={
     *                "members",
     *             }
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Возвращает информацию о голосовании",
     *         @SWG\Schema(ref="#/definitions/Voting"),
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */

    /**
     * @SWG\Post(
     *     path="/voting/{id}/member",
     *     summary="Выдвинуть свою кандидатуру на голосование",
     *     produces={"application/json", "application/xml"},
     *     tags={"Voting"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID голосования",
     *         type="integer",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Ok",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */
    public function actionCreateMember($id)
    {
        return '!!!';
    }

    /**
     * @SWG\Post(
     *     path="/voting/{idVote}/vote/{idMember}",
     *     summary="Голосование",
     *     produces={"application/json", "application/xml"},
     *     tags={"Voting"},
     *     @SWG\Parameter(
     *         name="idVote",
     *         in="path",
     *         description="ID голосования",
     *         type="integer",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="idMember",
     *         in="path",
     *         description="ID кандидата",
     *         type="integer",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Ok",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Voting not found",
     *     ),
     *     security={{"bearer":{}}}
     * )
     */
    public function actionVote($idVote, $idMember)
    {
        $user = Yii::$app->getUser()->getIdentity();
        $voting = Voting::findOne($idVote);

        if (!$voting) {
            throw new NotFoundHttpException('Initiative not found');
        }

        $isVote = VotingVote::findOne([
            'voting_id' => $idVote,
            'user_id' => $user->id,
            'member_user_id' => $idMember,
        ]);

        if ($user && $voting) {
            if (!$isVote) {
                $votingVote = new VotingVote([
                    'voting_id' => $idVote,
                    'user_id' => $user->id,
                    'member_user_id' => $idMember,
                ]);
                $votingVote->save();
            } else {
                return false;
            }
        }

        return true;
    }
}
