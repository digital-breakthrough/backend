<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;
?>

<div id="timeline">
    <?php Pjax::begin(); ?>
    <?php
    if (0<$dataProvider->getCount()) {
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'layout' => ""
                . "<div class='row'><div class='col-sm-4'>&nbsp;</div><div class='col-sm-4 text-center'>{pager}</div><div class='col-sm-4 text-center'>{summary}</div></div>"
                . "<ul class='timeline'>{items}</ul>" 
                . "<div class='row'><div class='col-sm-4'>&nbsp;</div><div class='col-sm-4 text-center'>{pager}</div><div class='col-sm-4 text-center'>{summary}</div></div>",
            'options' => [
                'tag' => 'div',
                // 'class' => 'timeline'
            ],
            'itemOptions' => [
                'tag' => 'li',
                'class' => 'item'
            ],
            'itemView' => '_item_view',
            'pager' => [
                'options' => [
                    'class' => 'pagination pagination-sm',
                ],
            ],
        ]);
    } else {
        echo '<p>Ничего не найдено.</p>';
    }
    ?>
    <?php Pjax::end(); ?>
</div>