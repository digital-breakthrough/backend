<?php $body = $model->getBody() ?>
<?php $footer = $model->getFooter() ?>

<i class="fa <?= $model->getIcon() ?>"></i>

<div class="timeline-item">
    <span class="time"><i class="fa fa-clock-o"></i> <?= Yii::$app->formatter->asDate($model->created_at)?> <?= Yii::$app->formatter->asTime($model->created_at)?></span>

    <h3 class="timeline-header"><?= $model->getHeader() ?></h3>

    <?php if ($body) : ?><div class="timeline-body"><?= $body ?></div><?php endif; ?>

    <?php if ($footer) : ?><div class="timeline-footer" style="background-color:lightgray;"><?= $footer ?></div><?php endif; ?>
</div>
