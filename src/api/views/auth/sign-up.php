<?php

use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\LoginForm */

$this->title = Yii::t('frontend', 'Sign Up');
$this->params['breadcrumbs'][] = $this->title;
$this->params['body-class'] = 'login-page';
?>
<div class="login-box" style="margin: 0 auto;">
    <div class="login-logo">
        <?php echo Html::encode($this->title) ?>
    </div><!-- /.login-logo -->
    <div class="header"></div>
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <div class="body">
            <?= $form->field($model, 'username', ['inputOptions' =>
                ['placeholder' => $model->getAttributeLabel('username')]
            ])->label(false) ?>

            <?= $form->field($model, 'email', ['inputOptions' =>
                ['placeholder' => $model->getAttributeLabel('email')]
            ])->label(false) ?>

            <?= $form->field($model, 'password', ['inputOptions' =>
                ['placeholder' => $model->getAttributeLabel('password')]
            ])->passwordInput()->label(false) ?>

            <?= Html::submitButton(Yii::t('frontend', 'Sign me up'), [
                'class' => 'btn btn-block btn-primary btn-flat btn-block',
                'name' => 'login-button'
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>