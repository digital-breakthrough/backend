<?php

namespace backend\assets;

use yii\web\AssetBundle;

class BowerAsset extends AssetBundle
{
    public $sourcePath = '@bower';

    public $js = [
        'jquery.timers/jquery.timers.min.js',
        'raphael/raphael.min.js',
        'morris.js/morris.min.js',
        'fancytree/dist/jquery.fancytree-all.min.js',
    ];

    public $css = [
        'morris.js/morris.css',
        'fancytree/dist/skin-bootstrap/ui.fancytree.min.css',
    ];
}
