var
    socket;

initSocket = function () {
    socket = new WebSocket(SocketServer);

    socket.onopen = function () {
        $('.sockets-status').addClass('active');
        console.log("Соединение установлено.");

        socket.send(JSON.stringify({
            'auth': userAuth
        }));
    };

    socket.onclose = function (event) {
        $('.sockets-status').removeClass('active');

        if (event.wasClean) {
            console.log('Соединение закрыто чисто');
        } else {
            console.log('Обрыв соединения'); // например, "убит" процесс сервера
        }
        console.log('Код: ' + event.code + ', причина: ' + event.reason);

        $('body')
            .stopTime()
            .everyTime(5000, function () {
                $('body').stopTime();
                initSocket();
            });
    };

    socket.onmessage = function (event) {
        // var
        //     data = JSON.parse(event.data);

        console.log(event.data);
    };

    socket.onerror = function (error) {
        console.log("Ошибка " + error.message);
    };
};

if (SocketServer !== undefined) {
    initSocket();
}
