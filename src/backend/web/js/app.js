var showModal = function (obj) {
    var target = obj.data('target'),
        url = obj.data('remote');

    $('.modal-content', target).load(
        url,
        function () {
            $(target).modal({show: true});
        }
    );
};

jQuery(document).ready(function () {
    $('.js-show-modal').on('click', function () {
        showModal($(this));
    })
});
