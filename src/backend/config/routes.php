<?php
return [
    'project/config/<id>' => 'project/config',
    'employee/config-jira/<projectId>' => 'employee/config-jira',
    'employee/config-redmine/<projectId>' => 'employee/config-redmine',
];
