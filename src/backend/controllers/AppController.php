<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Class AppController
 * @package backend\controllers
 */
class AppController extends Controller
{
    public $layout = 'common';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // Все страницы доступны только для залогиненых пользователей
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // Страницы "Авторизация", "Регистрация" доступны только для гостей
                    [
                        'controllers' => ['auth'],
                        'actions' => ['login', 'sign-up'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    // Страницы контроллера "site" доступны всем
                    [
                        'controllers' => ['site'],
                        'actions' => ['index', 'about', 'error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    // Страницы контроллера "secure" доступны всем
                    [
                        'controllers' => ['secure'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (Yii::$app->getUser()->isGuest) {
            $this->layout = 'main';
        }

        return parent::beforeAction($action);
    }
}
