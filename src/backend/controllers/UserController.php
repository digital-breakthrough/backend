<?php

namespace backend\controllers;

use backend\models\forms\{ConfigRedmineMembersForm, ConfigUserAuthForm, ConfigUserForm, LinkEntitiesForm};
use backend\services\EntityService;
use common\models\Entity;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class UserController
 * @package backend\controllers
 */
class UserController extends AppController
{
    /**
     * Displays homepage.
     * @return mixed
     */
    public function actionIndex()
    {
        $linkedUsers = Entity::find()
            ->where([
                'type' => Entity::TYPE_USER,
                'serviceId' => Entity::SERVICE_JIRA,
            ])->all();

        return $this->render('index', [
            'linkedUsers' => $linkedUsers,
        ]);
    }

    /**
     * @return array|string
     */
    public function actionCreate()
    {
        $formModel = new LinkEntitiesForm();

        if ($formModel->load(Yii::$app->request->post())) {
            Yii::$app->getResponse()->format = Response::FORMAT_JSON;
            return [
                'result' => $formModel->link(),
            ];
        } else {
            return $this->renderAjax('_create', [
                'jiraUsers' => (new EntityService())->getEntitiesAsMap(Entity::SERVICE_JIRA, Entity::TYPE_USER, true),
                'rmUsers' => (new EntityService())->getEntitiesAsMap(Entity::SERVICE_REDMINE, Entity::TYPE_USER, true),
                'formModel' => $formModel,
            ]);
        }
    }

    /**
     * @param $id
     * @return array|string
     */
    public function actionConfig($id)
    {
        $formModel = new ConfigUserForm([
            'serviceId' => $id
        ]);

        if ($formModel->load(Yii::$app->request->post(), '')) {
            Yii::$app->getResponse()->format = Response::FORMAT_JSON;
            return [
                'result' => $formModel->save(),
            ];
        } else {
            return $this->renderAjax('_config', [
                'users' => (new EntityService())->getEntities($id, Entity::TYPE_USER, false, false),
                'serviceId' => $id,
            ]);
        }
    }

    /**
     * @param $userId
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionConfigAuth($userId)
    {
        $user = Entity::findOne($userId);

        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }

        $formModel = new ConfigUserAuthForm([
            'userId' => $user->id,
        ]);

        if ($formModel->load(Yii::$app->request->post())) {
            $result = $formModel->save();

            Yii::$app->getResponse()->format = Response::FORMAT_JSON;
            return [
                'result' => $result,
            ];
        } else {

            return $this->renderAjax('_config-auth', [
                    'formModel' => $formModel
                ]
            );
        }
    }

}
