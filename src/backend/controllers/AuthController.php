<?php

namespace backend\controllers;

use Yii;
use yii\authclient\ClientInterface;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\{
    Account,
    LoginForm,
    UserAccount
};

/**
 * Class AuthController
 * @package backend\controllers
 */
class AuthController extends AppController
{
    public $layout = 'main';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::getBehaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'logout' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'client' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
                'successUrl' => Yii::$app->getHomeUrl() . 'accounts',
                'cancelCallback' => [$this, 'onAuthCancel'],
            ],
        ];
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @param ClientInterface $client
     */
    public function onAuthCancel(ClientInterface $client)
    {
    }

    /**
     * @param ClientInterface $client
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function onAuthSuccess(ClientInterface $client)
    {
        // Ищем аккаунт
        $account = Account::find()
            ->where([
                'type' => $client->getId(),
                'external_id' => $client->getUserId(),
            ])
            ->one();

        // Если аккаунт уже существует - обновляем
        if (!$account) {
            $account = new Account([
                'type' => $client->getId(),
                'php_class' => Account::getPhpClass($client->getId()),
                'external_id' => $client->getUserId(),
                'status' => Account::STATUS_UN_INIT,
            ]);
        } else {
            if (in_array($account->status, [Account::STATUS_DELETED, Account::STATUS_BLOCKED])) {
                $account->status = Account::STATUS_UN_INIT;
            }
        }

        $account->setAttributes([
            'php_class' => Account::getPhpClass($client->getId()),
            'external_link' => $client->getUserLink(),
            'external_title' => $client->getUserTitle(),
            'external_photo' => $client->getUserPhoto(),
            'token' => $client->getUserAccessToken(),
            'token_expires_in' => $client->getUserAccessTokenExpires(),
            'token_refresh' => $client->getUserRefreshCode(),
        ]);

        // Пытаемся сохранить в БД
        if ($account->validate()) {
            if ($account->save()) {
//                $queueClient = Yii::$app->get('queueAction')->getClient();
//                $queueClient->sendToQueue([
//                    'account' => $account->getApiAccount(),
//                    'action' => 'init'
//                ]);

                // Связываем с текущим пользователем
                $userAccount = UserAccount::findOne([
                    'id_account' => $account->getPrimaryKey(),
                    'id_user' => Yii::$app->getUser()->getIdentity()->getId(),
                ]);

                if (!$userAccount) {
                    $userAccount = new UserAccount([
                        'id_account' => $account->getPrimaryKey(),
                        'id_user' => Yii::$app->getUser()->getIdentity()->getId(),
                        'status' => Account::STATUS_UN_INIT,
                    ]);
                    $userAccount->save();
                }
            }
        } else {
            var_dump(
                $account->getAttributes(),
                $account->getErrors()
            );
        }
    }
}
