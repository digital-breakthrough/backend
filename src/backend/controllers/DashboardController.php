<?php

namespace backend\controllers;

/**
 * Class DashboardController
 * @package backend\controllers
 */
class DashboardController extends AppController
{
    /**
     * Displays homepage.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
