<?php
/**
 * Created by PhpStorm.
 * User: nkolesnikov
 * Date: 11.04.19
 * Time: 17:49
 */

namespace backend\services;

use common\interfaces\RequestTaskInterface;
use console\models\tasks\requests\jiraServiceDesk\getProjectUsers as JiraUsers;
use console\models\tasks\requests\redmine\getProjectUsers as RedmineUsers;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

class EmployeeService
{

    /**
     * Получить пользователей
     *
     * @return array
     */
    public function getUsers(): array
    {
        $regmineUsers = $this->getUsersbyIdProjects(
            \Yii::$app->params['settings']['mainRedmineProjectIds'],
            RedmineUsers::class
        );
        $jiraUsers = $this->getUsersbyIdProjects(
            \Yii::$app->params['settings']['mainJiraConfigurationSchemeIds'],
            JiraUsers::class
        );

        return [
            'redmineUsers' => $regmineUsers,
            'jiraUsers' => $jiraUsers,
        ];
    }

    /**
     * @param array $ids
     * @param string $className
     * @return array
     */
    private function getUsersbyIdProjects(array $ids, string $className): array
    {
        $users = [];
        foreach ($ids as $projectId) {
            /** @var RequestTaskInterface $command */
            $command = new $className(
                new Client(),
                [
                    'projectKey' => $projectId,
                ]
            );
            $users = ArrayHelper::merge($users, $command->getData());
        }
        return $users;
    }
}
