<?php

namespace backend\services;

use common\models\Entity;
use common\models\Mapping;
use yii\helpers\ArrayHelper;

/**
 * Class EntityService
 * @package backend\services
 */
class EntityService
{
    /**
     * @param $serviceId
     * @param $entityType
     * @param bool $excludeLinked
     * @param bool $onlyActive
     * @return array
     */
    public function getEntitiesAsMap($serviceId, $entityType, $excludeLinked = false, $onlyActive = true)
    {
        return ArrayHelper::map(self::getEntities($serviceId, $entityType, $excludeLinked, $onlyActive), 'id', 'externalTitle');
    }

    /**
     * @param $serviceId
     * @param $entityType
     * @param bool $excludeLinked
     * @param bool $onlyActive
     * @return array|Entity[]|\common\models\Task[]|\yii\db\ActiveRecord[]
     */
    public function getEntities($serviceId, $entityType, $excludeLinked = false, $onlyActive = true)
    {
        $entitiesQuery = Entity::find()
            ->where([
                'type' => $entityType,
                'serviceId' => $serviceId,
            ])->orderBy('id');

        if ($onlyActive) {
            $entitiesQuery->andWhere([
                'status' => Entity::STATUS_ACTIVE,
            ]);
        }

        if ($excludeLinked) {
            // Исключаем связанные
            $entitiesQuery->andWhere([
                'not in',
                'id',
                Entity::find()
                    ->innerJoin('relation', 'entity.id = relation.entity_id')
                    ->select(['id' => 'entity.id'])
            ]);

            $entitiesQuery->andWhere([
                'not in',
                'id',
                Entity::find()
                    ->innerJoin('relation', 'entity.id = relation.related_id')
                    ->select(['id' => 'entity.id'])
            ]);
        }

        return $entitiesQuery->all();
    }

    /**
     * @param $serviceId
     * @param $projectId
     * @param bool $asArray
     * @return array|Entity[]|\yii\db\ActiveRecord[]
     */
    public static function getEmployeesByProjects($serviceId, $projectId, $asArray = false)
    {
        $projectsQuery = Entity::find()
            ->where([
                'type' => Entity::TYPE_PROJECT_MEMBER,
                'serviceId' => $serviceId,
                'parentId' => $projectId,
            ])
            ->orderBy([
                'status' => SORT_ASC,
                'externalTitle' => SORT_ASC
            ]);

        if ($asArray) {
            $projectsQuery->asArray();
        }
        return $projectsQuery->all();
    }
}
