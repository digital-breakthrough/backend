<?php

use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\LoginForm */

$this->title = Yii::t('backend', 'Login');
$this->params['breadcrumbs'][] = $this->title;
$this->params['body-class'] = 'login-page';
?>
<div class="login-box" style="margin: 0 auto;">
    <div class="login-logo">
        <?php echo Html::encode($this->title) ?>
    </div><!-- /.login-logo -->
    <div class="header"></div>
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <div class="body">
            <?= $form->field($model, 'username', ['inputOptions' =>
                ['placeholder' => Yii::t('backend', 'Username or Email')]
            ])->label(false) ?>

            <?= $form->field($model, 'password', ['inputOptions' =>
                ['placeholder' => Yii::t('backend', 'Password')]
            ])->passwordInput()->label(false) ?>

            <div class="row">
                <div class="col-xs-8">
                    <?= $form->field($model, 'rememberMe')->checkbox(['class' => 'simple']) ?>
                </div>
                <div class="col-xs-4">
                    <?= Html::submitButton(Yii::t('backend', Yii::t('backend', 'Sign me in')), [
                        'class' => 'btn btn-primary btn-flat btn-block',
                        'name' => 'login-button'
                    ]) ?>
                </div>
            </div>

        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>