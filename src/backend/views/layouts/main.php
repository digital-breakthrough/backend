<?php
/**
 * @var $this yii\web\View
 */

use common\widgets\Alert;
use yii\helpers\Html;

$this->registerJs("var SocketServer = '" . env('SOCKET_HOST') . "';", $this::POS_HEAD);
$this->registerJs("var userAuth = '';", $this::POS_HEAD);

?>

<?php $this->beginContent('@backend/views/layouts/base.php'); ?>
    <div class="wrapper">
        <header class="main-header">
            <a href="/" class="logo">
                <img src="/img/logo.png" height="40" class="user-image" />
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li><?= Html::a(Yii::t('backend', 'About'), ['/site/about']) ?></li>
                    </ul>

                    <?php if(Yii::$app->user->isGuest) : ?>
                        <ul class="nav navbar-nav">
                            <li><?= Html::a(Yii::t('backend', 'Login'), ['/auth/login']) ?></li>
                            <li><?= Html::a(Yii::t('backend', 'Sign Up'), ['/auth/sign-up']) ?></li>
                        </ul>
                    <?php else :  ?>
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="https://dummyimage.com/100x100/000000/fff" class="user-image">
                                    <span><?php echo Yii::$app->user->identity->username ?> <i class="caret"></i></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header light-blue">
                                        <img src="https://dummyimage.com/100x100/000000/fff" class="img-circle" alt="User Image"/>
                                        <p><?php echo Yii::$app->user->identity->username ?></p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                        </div>
                                        <div class="pull-right">
                                            <?php echo Html::a(Yii::t('backend', 'Logout'), ['/auth/logout'], ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']) ?>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    <?php endif; ?>
                </div>
            </nav>
        </header>

        <aside class="content-wrapper no-margin">
            <section class="content">
                <?= Alert::widget() ?>

                <?= $content ?>
            </section><!-- /.content -->
        </aside>
    </div>
<?php $this->endContent(); ?>