<?php
/**
 * @var $this yii\web\View
 */

use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;
use backend\widgets\Menu;

$bundle = AppAsset::register($this);
$this->registerJs("var SocketServer = '" . env('SOCKET_HOST') . "';", $this::POS_HEAD);
$this->registerJs("var userAuth = '" . Yii::$app->getUser()->getIdentity()->getAuthKey() . "';", $this::POS_HEAD);
?>
<?php $this->beginContent('@backend/views/layouts/base.php'); ?>
<div class="wrapper">
    <!-- header logo: style can be found in header.less -->
    <header class="main-header">
        <a href="/" class="logo">
            <img src="/img/logo.png" height="40" class="user-image"/>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only"><?php echo Yii::t('backend', 'Toggle navigation') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <span class="sockets-status">
                <i class="fa fa-refresh"></i>
            </span>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li><?= Html::a(Yii::t('backend', 'About'), ['/site/about']) ?></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="https://dummyimage.com/100x100/000000/fff" class="user-image">
                            <span><?php echo Yii::$app->user->identity->username ?> <i class="caret"></i></span>
                        </a>

                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header light-blue">
                                <img src="https://dummyimage.com/100x100/000000/fff" class="img-circle"
                                     alt="User Image"/>
                                <p><?php echo Yii::$app->user->identity->username ?></p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                </div>
                                <div class="pull-right">
                                    <?php echo Html::a(Yii::t('backend', 'Logout'), ['/auth/logout'], ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="https://dummyimage.com/100x100/000000/fff" class="img-circle"/>
                </div>
                <div class="pull-left info">
                    <p><?php echo Yii::t('backend', 'Hello, {username}', ['username' => Yii::$app->user->identity->username]) ?></p>
                    <a href="<?php echo Url::to(['/sign-in/profile']) ?>">
                        <i class="fa fa-circle text-success"></i>
                        <?php echo Yii::$app->formatter->asDatetime(time()) ?>
                    </a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <?php echo Menu::widget([
                'options' => ['class' => 'sidebar-menu'],
                'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                'activateParents' => true,
                'items' => [
                    [
                        'label' => Yii::t('backend', 'Main'),
                        'options' => ['class' => 'header'],
                    ],
                    [
                        'label' => Yii::t('backend', 'Dashboard'),
                        'url' => ['/dashboard/index'],
                        'icon' => '<i class="fa fa-table"></i>',
                        'active' => Yii::$app->controller->id === 'dashboard',
                    ],
                    [
                        'label' => Yii::t('backend', 'Tasks'),
                        'url' => ['/task/index'],
                        'icon' => '<i class="fa fa-refresh"></i>',
                        'active' => Yii::$app->controller->id === 'task',
                    ],
                    [
                        'label' => Yii::t('backend', 'Timeline'),
                        'url' => ['/timeline/index'],
                        'icon' => '<i class="fa fa-tasks"></i>',
                        'active' => Yii::$app->controller->id === 'timeline',
                    ],
                    [
                        'label' => Yii::t('backend', 'Settings'),
                        'url' => '#',
                        'icon' => '<i class="fa fa-cog"></i>',
                        'options' => ['class' => 'treeview'],
                        'active' => in_array(Yii::$app->controller->id, [
                            'project',
                            'employee',
                            'user',
                        ]),
                        'items' => [
                            [
                                'label' => Yii::t('backend', 'Projects'),
                                'url' => ['/project/index'],
                                'icon' => '<i class="fa fa-caret-right"></i>',
                                'active' => Yii::$app->controller->id === 'project',
                            ],
                            [
                                'label' => Yii::t('backend', 'Users'),
                                'url' => ['/user/index'],
                                'icon' => '<i class="fa fa-caret-right"></i>',
                                'active' => Yii::$app->controller->id === 'user',
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('backend', 'Server'),
                        'url' => '#',
                        'icon' => '<i class="fa fa-server"></i>',
                        'active' => Yii::$app->controller->id === 'server',
                        'options' => ['class' => 'treeview'],
                        'items' => [
                            [
                                'label' => Yii::t('backend', 'Info'),
                                'url' => ['/server/info'],
                                'icon' => '<i class="fa fa-caret-right"></i>',
                                'active' => Yii::$app->controller->action->id === 'info',
                            ],
                            [
                                'label' => Yii::t('backend', 'Cron'),
                                'url' => ['/server/cron'],
                                'icon' => '<i class="fa fa-caret-right"></i>',
                                'active' => Yii::$app->controller->action->id === 'cron',
                            ],
                        ],
                    ],
                ]
            ]) ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $this->title ?>
                <?php if (isset($this->params['subtitle'])): ?>
                    <small><?php echo $this->params['subtitle'] ?></small>
                <?php endif; ?>
            </h1>

            <?php echo Breadcrumbs::widget([
                'tag' => 'ol',
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?= Alert::widget() ?>

            <?= $content ?>
        </section><!-- /.content -->
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->
<?php $this->endContent(); ?>
