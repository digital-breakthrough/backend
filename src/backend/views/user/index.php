<?php

use common\models\Entity;
use yii\bootstrap\Button;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $configMenu array */
/* @var $linkedUsers array */

$this->title = Yii::t('backend', 'Linked Users');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-xs-6">
        <?= Button::widget([
            'label' => Yii::t('backend', 'New link for users'),
            'options' => [
                'class' => 'btn btn-md btn-primary js-show-modal',
                'data-target' => '#users-modal',
                'data-remote' => Url::toRoute(['/user/create']),
            ],
        ]); ?>

        <?= ButtonDropdown::widget([
            'label' => Yii::t('backend', 'Config users'),
            'options' => [
                'class' => 'btn btn-md btn-default',
            ],
            'dropdown' => [
                'items' => [
                    [
                        'label' => 'Jira Service Desk',
                        'url' => '#',
                        'linkOptions' => [
                            'data-target' => '#users-modal',
                            'data-remote' => Url::toRoute(['/user/config', 'id' => Entity::SERVICE_JIRA]),
                            'class' => 'js-show-modal',
                        ],
                    ],
                    [
                        'label' => 'Redmine',
                        'url' => '#',
                        'linkOptions' => [
                            'data-target' => '#users-modal',
                            'data-remote' => Url::toRoute(['/user/config', 'id' => Entity::SERVICE_REDMINE]),
                            'class' => 'js-show-modal',
                        ],
                    ],
                ],
            ]
        ]); ?>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col" style="text-align:right;">Jira SD Users</th>
                        <th>&nbsp;</th>
                        <th scope="col" style="text-align:center;" width="100">Direction</th>
                        <th>&nbsp;</th>
                        <th scope="col">Redmine Users</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    /** @var Entity $item */
                    foreach ($linkedUsers as $item) : ?>
                        <?php if ($item->relatedEntity) : ?>
                            <tr>
                                <td width="50%" align="right">
                                    <b><a href="<?= $item->externalUrl ?>" target="_blank"
                                          style="line-height:30px;"><?= $item->externalTitle ?></a></b>
                                </td>
                                <td>
                                    <?= Html::button(
                                        '<i class="fa fa-cog"></i>',
                                        [
                                            'data-target' => '#users-modal',
                                            'data-remote' => Url::toRoute(['/user/config-auth', 'userId' => $item->id]),
                                            'class' => 'js-show-modal btn btn-sm btn-default',
                                        ]); ?>
                                </td>
                                <td align="center"><span class="fa fa-angle-right" style="line-height:30px;"></span>
                                </td>
                                <td>
                                    <?= Html::button(
                                        '<i class="fa fa-cog"></i>',
                                        [
                                            'data-target' => '#users-modal',
                                            'data-remote' => Url::toRoute(['/user/config-auth', 'userId' => $item->relatedEntity->id]),
                                            'class' => 'js-show-modal btn btn-sm btn-default',
                                        ]); ?>
                                </td>
                                <td width="50%">
                                    <a href="<?= $item->relatedEntity->externalUrl ?>" target="_blank"
                                       style="line-height:30px;"><?= $item->relatedEntity->externalTitle ?></a>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?= Modal::widget([
    'id' => 'users-modal',
    'toggleButton' => false,
    'clientOptions' => false,
]); ?>
