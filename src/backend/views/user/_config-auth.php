<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $formModel \backend\models\forms\ConfigUserAuthForm */

?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?= Yii::t('backend', 'Config for user <b>`' . $formModel->user->externalTitle . '`</b>') ?></h4>
    </div>

<?php $form = ActiveForm::begin(['id' => 'config-auth-form', 'action' => Url::toRoute(['/user/config-auth', 'userId' => $formModel->userId])]); ?>
    <div class="modal-body">
        <?= $form->field($formModel, 'login')->textInput([
            'placeholder' => 'Type login here...',
            'class' => 'form-control'
        ]) ?>
        <?= $form->field($formModel, 'password')->passwordInput([
            'placeholder' => 'Type password here...',
            'class' => 'form-control'
        ]) ?>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('backend', 'Cancel') ?></button>
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary',]) ?>
    </div>
<?php ActiveForm::end(); ?>

<?php
$js = <<<JS
    $('#config-auth-form').on('beforeSubmit', function(){
        var data = $(this).serialize();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: 'POST',
            data: data,
            success: function(res){
            console.log(res);
                location.reload();
            },
            error: function(){
                console.log('Error');
            }
        });
        return false;
    });
JS;

$this->registerJs($js);
?>
