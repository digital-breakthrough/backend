<?php

use common\models\Entity;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $users array */
/* @var $serviceId string */

?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?= Yii::t('backend', 'All users in service <b>`' . $serviceId . '`</b>') ?></h4>
    </div>

<?php $form = ActiveForm::begin(['id' => 'config-form', 'action' => Url::toRoute(['/user/config', 'id' => $serviceId])]); ?>
    <div class="modal-body">
        <?php /**@var Entity $item */ ?>
        <?php foreach ($users as $item) : ?>
            <div class="checkbox">
                <label <?= ($item->relatedEntity) ? 'style="font-weight:bold;"' : '' ?>>
                    <input name="users[]" value="<?= $item->id ?>"
                           type="checkbox"
                        <?= ($item->status === Entity::STATUS_ACTIVE) ? 'checked' : '' ?>
                    > <?= $item->externalTitle ?>
                </label>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('backend', 'Cancel') ?></button>
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary',]) ?>
    </div>
<?php ActiveForm::end(); ?>

<?php
$js = <<<JS
    $('#config-form').on('beforeSubmit', function(){
        var data = $(this).serialize();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            type: 'POST',
            data: data,
            success: function(res){
            console.log(res);
                location.reload();
            },
            error: function(){
                console.log('Error');
            }
        });
        return false;
    });
JS;

$this->registerJs($js);
?>