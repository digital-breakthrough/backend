<?php

/* @var $this \yii\web\View */
/* @var $jiraUsers array */
/* @var $rmUsers array */

/* @var $formModel \backend\models\forms\LinkEntitiesForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?= Yii::t('backend', 'New link for users') ?></h4>
</div>

<?php $form = ActiveForm::begin(['id' => 'create-form']); ?>
    <div class="modal-body">
        <div class="row">
            <label class="col-sm-5 control-label">Jira SD User</label>
            <label class="col-sm-2 control-label">Direction</label>
            <label class="col-sm-5 control-label">Redmine User</label>
        </div>

        <div class="row">
            <div class="col-sm-5">
                <?= $form->field($formModel, 'jiraId')->dropDownList($jiraUsers, ['prompt' => 'Select user'])->label(false) ?>
            </div>

            <div class="col-sm-2 text-center" style="line-height: 34px;">
                <span class="fa fa-2x fa-angle-right"></span>
            </div>

            <div class="col-sm-5">
                <?= $form->field($formModel, 'redmineId')->dropDownList($rmUsers, ['prompt' => 'Select user'])->label(false) ?>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('backend', 'Cancel') ?></button>
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary',]) ?>
    </div>
<?php ActiveForm::end(); ?>

<?php
$js = <<<JS
    $('#create-form').on('beforeSubmit', function(){
        var data = $(this).serialize();
        $.ajax({
            url: '/project/create',
            type: 'POST',
            data: data,
            success: function(res){
                location.reload();
            },
            error: function(){
                console.log('Error');
            }
        });
        return false;
    });
JS;

$this->registerJs($js);
?>